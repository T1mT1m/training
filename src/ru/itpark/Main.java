package ru.itpark;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Store store = new Store();
        while (true) {
            System.out.println("Ввести команду");
            System.out.println("a - добавить продукт");
            System.out.println("l - вывести список продуктов");
            System.out.println("x - удалить продукт по индексу");
            System.out.println("q - выход");

            String command = scanner.next();
            switch (command) {
                case "a":
                    System.out.println("Введите идентификатор");
                    int id = scanner.nextInt();
                    System.out.println("Введите название");
                    String name = scanner.next();
                    System.out.println("Введите стоимость");
                    int price = scanner.nextInt();
                    store.add(new Product(id, name, price));
                    break;
                case "l":
                    System.out.println(store.getAll());
                    break;
                case "x":
                    System.out.println("Введите индкс товара");
                    int index = scanner.nextInt();
                    store.remove(index);

                case "q":
                    return;
                    default:
                        System.out.println("Команда не распознала");
            }
        }
    }

}
